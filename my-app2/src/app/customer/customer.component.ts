import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-root',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  name: string;
  age: number;
  email: string;
  salary: number;
  message: string;

  constructor(private customerService: CustomerService) {
    this.name = 'George';
    this.age = 100;
    this.email = 'test@test.com';

    this.message = 'Message from parent';
  }

  ngOnInit() {
  }

  clickMe(): void {
    console.log('clicked');
    this.customerService.getCustomer()
    .toPromise()
    .then((response) => { // success
      console.log(response);
      this.name = response[0].name;
    })
    .catch((error) => { // error
      console.log(error);
    });
  }

}

