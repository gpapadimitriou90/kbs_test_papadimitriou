import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class CustomerService {

  constructor(private http: HttpClient) { }

  displayLog(): void {
    console.log('This is a service function');
  }

  getCustomer(): Observable<any> {
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }
}
