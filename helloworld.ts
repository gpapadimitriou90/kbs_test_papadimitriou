interface IBook {
    title: string;
    authors: any[];
    isFiction?: boolean;
    print(): void;
}

interface IAuthor {
    first_name: string;
    last_name: string;
    age?: number;
    print(): void;
}


class Book implements IBook {
    title: string;
    authors: any[];

    constructor(title: string, authors: any[]) {
        this.title = title;
        this.authors = authors;
    }

    print(): void {
        console.log('The book\'s title is: ' + this.title);
        console.log(`The book's title is: ${this.title}`);
    }

}

class Author implements IAuthor {
    first_name: string;
    last_name: string;

    constructor(first_name: string, last_name: string) {
        this.first_name = first_name;
        this.last_name = last_name;
    }

    print(): void {
        console.log('The author\'s name is: ' + this.first_name);
    }

}



let author1 = new Author('J.R.R', 'Tolkien');
author1.print();
let book1 = new Book('The Hobbit', [author1]);
book1.print();

